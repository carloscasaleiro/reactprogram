function BookStorage(ownerName) {
    this.ownerName = ownerName;
    this.favoriteBooks = [];
  }
  
  BookStorage.prototype.addFavoriteBook = function(bookName) {
    if (!bookName.includes("Great")) {
      this.favoriteBooks.push(bookName);
    }
  };
  
  BookStorage.prototype.printFavoriteBooks = function() {
    console.log(`${this.ownerName}'s Favorite Books: ${String(this.favoriteBooks.length)}`);
    for (let bookName of this.favoriteBooks) {
      console.log(bookName);
    }
  };
  
  BookStorage.prototype.sortFavoriteBooksByAlphabeticDescendingOrder = function() {
    this.favoriteBooks.sort((a, b) => b.localeCompare(a));
  };
  
  function Bookshelf(ownerName) {
    BookStorage.call(this, ownerName);
  }
  
  Bookshelf.prototype = Object.create(BookStorage.prototype);
  
  function Cardbox(ownerName) {
    BookStorage.call(this, ownerName);
  }
  
  Cardbox.prototype = Object.create(BookStorage.prototype);

  export { Bookshelf, Cardbox };