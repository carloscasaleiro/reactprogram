import { Bookshelf, Cardbox } from './ex.inheritance.js';

function loadBooks(bookshelf) {
  fakeAjax(BOOK_API, function onBooks(bookNames) {
    for (let bookName of bookNames) {
      bookshelf.addFavoriteBook(bookName);
    }
    bookshelf.sortFavoriteBooksByAlphabeticDescendingOrder();
    bookshelf.printFavoriteBooks();
    console.log("\n");
  });
}

var BOOK_API = "https://some.url/api";

var myBooks = new Bookshelf("Jack");
loadBooks(myBooks);

var cardboxBooks = new Cardbox("Paul");
loadBooks(cardboxBooks);

// ***********************

// NOTE: don't modify this function at all
function fakeAjax(url, cb) {
  setTimeout(function fakeLoadingDelay() {
    cb([
      "A Song of Ice and Fire",
      "The Great Gatsby",
      "Crime & Punishment",
      "Great Expectations",
      "You Don't Know JS",
    ]);
  }, 500);
}
