function Bookshelf(ownerName) {
  this.ownerName = ownerName;
  this.favoriteBooks = [];
}

Bookshelf.prototype.addFavoriteBook = function(bookName) {
  if (!bookName.includes("Great")) {
    this.favoriteBooks.push(bookName);
  }
}

Bookshelf.prototype.printFavoriteBooks = function() {
  console.log(`${this.ownerName}'s Favorite Books: ${String(this.favoriteBooks.length)}`);
  for (let bookName of this.favoriteBooks) {
    console.log(bookName);
  }
}

Bookshelf.prototype.sortFavoriteBooksByAlphabeticDescendingOrder = function() {
  this.favoriteBooks.sort((a, b) => b.localeCompare(a));
}

function loadBooks(bookshelf) {
  fakeAjax(BOOK_API, function onBooks(bookNames) {
    for (let bookName of bookNames) {
      bookshelf.addFavoriteBook(bookName);
    }
    bookshelf.sortFavoriteBooksByAlphabeticDescendingOrder();
    bookshelf.printFavoriteBooks();
    console.log("\n");
  });
}

var BOOK_API = "https://some.url/api";
var myBooks = new Bookshelf("Jack");
loadBooks(myBooks);


/**
 * CARDBOX 
 */
function Cardbox(ownerName) {
  //this.ownerName = ownerName;
  //this.favoriteBooks = [];
  Bookshelf.call(this, ownerName);
}

Cardbox.prototype = Object.create(Bookshelf.prototype);
var cardboxBooks = new Cardbox("Paul");
loadBooks(cardboxBooks);

// ***********************

// NOTE: don't modify this function at all
function fakeAjax(url, cb) {
  setTimeout(function fakeLoadingDelay() {
    cb([
      "A Song of Ice and Fire",
      "The Great Gatsby",
      "Crime & Punishment",
      "Great Expectations",
      "You Don't Know JS",
    ]);
  }, 500);
}
